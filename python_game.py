import pygame, sys, random, time, additional_features

error_check = pygame.init()
game_surface = pygame.display.set_mode((720, 460))
pygame.display.set_caption("Python Game")

#Colors
BLACK = pygame.Color(0, 0, 0)
GREEN = pygame.Color(0, 255, 0)
RED = pygame.Color(255, 0, 0)
WHITE = pygame.Color(255, 255, 255)

fps_obj=pygame.time.Clock()
fps=25

def game_init():
    global snake_body, snake_pos, food_pos, score, direction
    score = 0
    snake_pos = [200, 50]
    snake_body = [[200, 50], [190, 50], [180, 50]]
    food_pos = [random.randrange(1,72)*10, random.randrange(1,46)*10]
    direction = 'RIGHT'
    game_logic()

def score_display():
    s_font = pygame.font.SysFont('manaco', 24)
    s_surface = s_font.render('Score : ' + str(score), True , WHITE)
    #s_rectangle = s_surface.get_rect()
    #s_rectangle.midtop = (360, 10)
    #s_surface.blit(s_surface, s_rectangle)
    game_surface.blit(s_surface,(10,10))
    pygame.display.flip()


def game_over():
    my_font = pygame.font.SysFont('manaco', 72)
    Game_Over_Surface = my_font.render('Game Over !!!', True , RED)
    Game_Over_rectangle = Game_Over_Surface.get_rect()
    Game_Over_rectangle.midtop = (360 , 180)
    game_surface.blit(Game_Over_Surface, Game_Over_rectangle)
    pygame.display.flip()
    time.sleep(4)
    pygame.quit() #pygame exit
    sys.exit() 

def game_logic():
    global direction, score, food_pos
    while True:
        #Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            #Handling the key pressed
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    if direction != 'LEFT':
                        direction = 'RIGHT'
                elif event.key == pygame.K_LEFT:
                    if direction != 'RIGHT':
                        direction = 'LEFT'
                elif event.key == pygame.K_UP:
                    if direction != 'DOWN':
                        direction = 'UP'
                elif event.key == pygame.K_DOWN:
                    if direction != 'UP':
                        direction = 'DOWN'
            
        if direction == 'RIGHT':
            snake_pos[0] += 10
        if direction == 'LEFT':
            snake_pos[0] -= 10
        if direction == 'UP':
            snake_pos[1] -= 10
        if direction == 'DOWN':
            snake_pos[1] += 10

        #Updating snake movement
        snake_body.insert(0, list(snake_pos))

        game_surface.fill(BLACK)

        #Checking if food eaten or not
        if (snake_pos[0],snake_pos[1]) == (food_pos[0],food_pos[1]):
            score += 1
            food_pos = [random.randrange(1, 72) * 10, random.randrange(1, 46) * 10]
        else:
            snake_body.pop()

        for pos in snake_body:
            pygame.draw.rect(game_surface, GREEN, pygame.Rect(pos[0], pos[1], 10, 10))

        pygame.draw.rect(game_surface, RED, pygame.Rect(food_pos[0], food_pos[1], 10, 10))

        score_display()

        #Hit checks
        if snake_pos[0] > 710 or snake_pos[0] < 0:
            game_over()
        if snake_pos[1] > 450 or snake_pos[1] < 0:
            game_over()
        if len(snake_body) > 4:
            for body in snake_body[1:]:
                if snake_pos[0] == body[0] and snake_pos[1] == body[1]:
                    game_over()
        

        fps_obj.tick(fps)
        pygame.display.flip()            


if error_check[1] > 0:
    print("Error while initializing pygame.So exiting !")
else:
    print("Initialization successful")
    game_init()
    