import pygame, sys, random, time

error_check = pygame.init()
game_surface = pygame.display.set_mode((720, 460))
pygame.display.set_caption("Python Game")

#Colors
BLACK = pygame.Color(0, 0, 0)
GREEN = pygame.Color(0, 255, 0)
RED = pygame.Color(255, 0, 0)
WHITE = pygame.Color(255, 255, 255)
BLUE = pygame.Color(0, 100, 255)

fps_obj=pygame.time.Clock()
fps=20

class snake:
    score = 0
    snake_pos = []
    snake_body = []
    direction = 'RIGHT'
    health = []
    power = False
    w = 10
    def check_food_eaten():
        if(snake_pos[0],snake_pos[1]) == (food_pos[0],food_pos[1]) : 
            score += 1    
            food_pos = [random.randrange(9, 71) * 10, random.randrange(2, 44) * 10]
        elif (snake_pos[0],snake_pos[1]) == (food_pos[0],food_pos[1]) :
            score += 1    
            food_pos = [random.randrange(9, 71) * 10, random.randrange(2, 44) * 10]
        else :
            snake_body.pop()

snake1 = snake()
snake2 = snake()

class food:
    food_pos = [random.randrange(9, 71) * 10, random.randrange(2, 44) * 10]

food1 = food()
food2= food()

def game_init():
    snake1.snake_pos = [200, 50]
    snake1.snake_body = [[200, 50], [190, 50], [180, 50]]
    snake2.snake_pos = [500, 400]
    snake2.snake_body = [[500, 400],[510, 400],[520, 400]]
    snake2.direction = 'LEFT'
    #snake1.health = [[20, 27], [40, 27], [60, 27], [80, 27], [100, 27]]
    #snake2.health = [[580, 27], [600, 27], [620, 27], [640, 27], [660, 27]]
    game_logic()

def check_winner():
    if snake1.score >= 30 : #or len(snake2.health) == 0
        game_over(1)
    elif snake2.score >= 30 :# or len(snake1.health) == 0
        game_over(2)

def power_up():
    if snake1.score > 4:
        snake1.power = True
        snake1.w = 30
    if snake2.score > 4:
        snake2.power = True
        snake2.w = 30
    if snake1.power:
        for body in snake2.snake_body:
            if snake1.snake_pos[0] == body[0] and snake1.snake_pos[1] == body[1]:
                #snake2.health.pop()
                snake2.score -= 5
                break
    pygame.display.flip()
    if snake2.power:
        for body in snake1.snake_body:
            if snake2.snake_pos[0] == body[0] and snake2.snake_pos[1] == body[1]:
                #snake1.health.pop()
                snake1.score -=5
                break
    pygame.display.flip()

def score_display():
    s_font = pygame.font.SysFont('manaco', 24)
    s_surface = s_font.render('Player 1 Score : ' + str(snake1.score), True , BLUE)
    s2_surface = s_font.render('Player 2 Score : ' + str(snake2.score), True , GREEN)
    game_surface.blit(s_surface,(20,10))
    game_surface.blit(s2_surface,(580,10))
    check_winner()
    power_up()
    pygame.display.flip()
def game_over(player):
    my_font = pygame.font.SysFont('manaco', 72)
    Game_Over_Surface = my_font.render('Game Over ! Player '+ str(player) + ' won', True , RED)
    Game_Over_rectangle = Game_Over_Surface.get_rect()
    Game_Over_rectangle.midtop = (360 , 180)
    game_surface.blit(Game_Over_Surface, Game_Over_rectangle)
    pygame.display.flip()
    time.sleep(2)
    pygame.quit() #pygame exit
    sys.exit() 

def game_logic():
    game_surface.fill(BLACK)
    while True:
        #Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            #Handling the key pressed
            elif event.type == pygame.KEYDOWN:
                #Handling W A S D Keys
                if event.key == 100:
                    if snake1.direction != 'LEFT':
                        snake1.direction = 'RIGHT'
                elif event.key == 97:
                    if snake1.direction != 'RIGHT':
                        snake1.direction = 'LEFT'
                elif event.key == 119:
                    if snake1.direction != 'DOWN':
                        snake1.direction = 'UP'
                elif event.key == 115:
                    if snake1.direction != 'UP':
                        snake1.direction = 'DOWN'
                #Handling Arrow Keys
                if event.key == pygame.K_RIGHT:
                    if snake2.direction != 'LEFT':
                        snake2.direction = 'RIGHT'
                elif event.key == pygame.K_LEFT:
                    if snake2.direction != 'RIGHT':
                        snake2.direction = 'LEFT'
                elif event.key == pygame.K_UP:
                    if snake2.direction != 'DOWN':
                        snake2.direction = 'UP'
                elif event.key == pygame.K_DOWN:
                    if snake2.direction != 'UP':
                        snake2.direction = 'DOWN'

        if snake2.direction == 'RIGHT':
            snake2.snake_pos[0] += 10
        if snake2.direction == 'LEFT':
            snake2.snake_pos[0] -= 10
        if snake2.direction == 'UP':
            snake2.snake_pos[1] -= 10
        if snake2.direction == 'DOWN':
            snake2.snake_pos[1] += 10

        if snake1.direction == 'RIGHT':
            snake1.snake_pos[0] += 10
        if snake1.direction == 'LEFT':
            snake1.snake_pos[0] -= 10
        if snake1.direction == 'UP':
            snake1.snake_pos[1] -= 10
        if snake1.direction == 'DOWN':
            snake1.snake_pos[1] += 10

        #Updating snake movement
        snake2.snake_body.insert(0, list(snake2.snake_pos))
        snake1.snake_body.insert(0, list(snake1.snake_pos))

        game_surface.fill(BLACK)

        #Displaying Food
        pygame.draw.rect(game_surface, GREEN, pygame.Rect(food2.food_pos[0], food2.food_pos[1], 10, 10))
        pygame.draw.rect(game_surface, BLUE, pygame.Rect(food1.food_pos[0], food1.food_pos[1], 10, 10))
        
        #Checking if food eaten or not
        if(snake2.snake_pos[0],snake2.snake_pos[1]) == (food2.food_pos[0],food2.food_pos[1]) : 
            snake2.score += 1    
            food2.food_pos = [random.randrange(9, 71) * 10, random.randrange(2, 44) * 10]
        elif (snake2.snake_pos[0],snake2.snake_pos[1]) == (food1.food_pos[0],food1.food_pos[1]) :
            snake2.score += 1    
            food1.food_pos = [random.randrange(9, 71) * 10, random.randrange(2, 44) * 10]
        else :
            snake2.snake_body.pop()

        if (snake1.snake_pos[0],snake1.snake_pos[1]) == (food2.food_pos[0],food2.food_pos[1]):
            snake1.score += 1
            food2.food_pos = [random.randrange(9, 71) * 10, random.randrange(2, 44) * 10]
        elif (snake1.snake_pos[0],snake1.snake_pos[1]) == (food1.food_pos[0],food1.food_pos[1]):
            snake1.score += 1
            food1.food_pos = [random.randrange(9, 71) * 10, random.randrange(2, 44) * 10]
        else:
            snake1.snake_body.pop()

        #Displaying Food
        pygame.draw.rect(game_surface, GREEN, pygame.Rect(food2.food_pos[0], food2.food_pos[1], 10, 10))
        pygame.draw.rect(game_surface, BLUE, pygame.Rect(food1.food_pos[0], food1.food_pos[1], 10, 10))

        #Displaying Snake body
        for pos in snake2.snake_body:
            pygame.draw.rect(game_surface, GREEN, pygame.Rect(pos[0], pos[1], snake2.w, snake2.w))
        for pos in snake1.snake_body:
            pygame.draw.rect(game_surface, BLUE, pygame.Rect(pos[0], pos[1], snake1.w, snake1.w))
        
        #Displaying health bar
        for h in snake1.health:
            pygame.draw.rect(game_surface, BLUE, pygame.Rect(h[0], h[1], 15, 15))
        for h in snake2.health:
            pygame.draw.rect(game_surface, GREEN, pygame.Rect(h[0], h[1], 15, 15))
        
        #Boundary Hit Check
        if snake2.snake_pos[0] > 710 or snake2.snake_pos[0] < 0:
            game_over(1)
        if snake2.snake_pos[1] > 450 or snake2.snake_pos[1] < 0:
            game_over(1)
        if snake1.snake_pos[0] > 710 or snake1.snake_pos[0] < 0:
            game_over(2)
        if snake1.snake_pos[1] > 450 or snake1.snake_pos[1] < 0:
            game_over(2)
        score_display()
        if len(snake2.snake_body) > 4:
            for body in snake2.snake_body[1:]:
                if snake2.snake_pos[0] == body[0] and snake2.snake_pos[1] == body[1]:
                    game_over(1)
        if len(snake1.snake_body) > 4:
            for body in snake1.snake_body[1:]:
                if snake1.snake_pos[0] == body[0] and snake1.snake_pos[1] == body[1]:
                    game_over(2)

        fps_obj.tick(fps)
        pygame.display.flip()            

if error_check[1] > 0:
    print("Error while initializing pygame.So exiting !")
else:
    print("Initialization successful")
    game_init()
